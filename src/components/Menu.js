import React from 'react'

const MENU = [
  {
    name: 'Sopas',
    items: [
      {
        name: 'Tabira',
        description: 'Sopa de feijão com bacon, calabresa, charque, carne de sol e legumes'
      },
      {
        name: 'Pescado Porto de Galinhas',
        description: 'Sopa de pescado amarelo com macarrão e legumes'
      },
      {
        name: 'Camaragibe',
        description: 'Sopa feita com milho, diversas carnes e legumes'
      },
      {
        name: 'Mocotó Casa Amarela',
        description: 'Sopa de mocotó com legumes'
      },
      {
        name: 'Verdinha do Marco Zero',
        description: 'Sopa vegana de brócolis, couve flor e legumes'
      }
    ]
  },
  {
    name: 'Cremes',
    img: '/imgs/category-cream.jpg',
    items: [
      {
        name: 'Ervilha de Garanhuns',
        description: 'Creme de ervilha com bacon, linguiça, legumes e temperos batidos'
      },
      {
        name: 'Amansa Corno de Serra Talhada',
        description: 'Creme de abobora com linguiça e queijo coalho'
      },
      {
        name: 'Cara de vitória de santo antão',
        description: 'Creme de cará com carne e queijo mussarela'
      },
      {
        name: 'Canja de Exú',
        description: 'Canja de galinha tradicional'
      },
      {
        name: 'Quenga da avenida Norte',
        description: 'Caldo de quenga tradicional com galinha e legumes'
      }
    ]
  },
  {
    name: 'Sopas doces',
    img: '/imgs/category-desert.jpeg',
    items: [
      {
        name: 'Sex hot de Caruaru',
        description: 'Sopa de morangos com espumante reduzido em especiarias servido com sorvete de creme'
      },
      {
        name: 'Jaboatão dos Chocolates',
        description: 'Sopa de chocolate meio amargao com leite e raspas de laranja com fruntas para acompanha'
      }
    ]
  }
]

export const Menu = () => {
  const renderMenuCategories = () => {
    return MENU.map(category => (
      <div className="category">
        <hr className="divider"/>
        {
          category.img && (
            <div className="parallax" style={{ backgroundImage: `url(${category.img})` }}></div>
          )
        }
        <h2 className="category header">{ category.name }</h2>
        { renderMenuItems(category.items) }
      </div>
    ))
  }

  const renderMenuItems = (items) => {
    return items.map(item => (
        <div className="row item">
          <div className="col-md-7">
            <h3 className="header">
              { item.name }
            </h3>
            <p className="description">{ item.description }</p>
          </div>
        </div>
    ))
  }

  return (
    <div className="menu">
      { renderMenuCategories() }
    </div>
  )
}
