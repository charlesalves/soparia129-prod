import React from 'react'

export const MasterHeader = () => {
  return (
    <section className="py-5 text-center w-100 masterhead parallax" style={{ backgroundImage: "url('/imgs/category-soup.jpeg')" }}>
      <div className="row py-lg-5 masterhead-logo">
        <div className="col-lg-6 col-md-8 mx-auto">
          <h1 className="fw-light">Sempre o ideal</h1>
          <p className="lead">Sopas e cremes regionais</p>
        </div>
      </div>
    </section>
  )
}
